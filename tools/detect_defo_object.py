#!/usr/bin/env python

import _init_paths
from fast_rcnn.config import cfg
from fast_rcnn.test import im_detect
from fast_rcnn.nms_wrapper import nms
from utils.timer import Timer
import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio
import caffe, os, sys, cv2
import argparse
from PIL import Image, ImageDraw

CLASSES = ('__background__', 'object_0','object_1','object_2','object_3','object_4')

def parse_args():
	"""Parse input arguments."""
	parser = argparse.ArgumentParser(description='Train a Fast R-CNN network')
	parser.add_argument('--gpu', dest='gpu_id', help='GPU device id to use [0]',
						default=0, type=int)
	parser.add_argument('--cpu', dest='cpu_mode',
						help='Use CPU mode (overrides --gpu)',
						action='store_true')
	parser.add_argument('--path', dest='test_path',
						help = 'The path for test images')
	parser.add_argument('--prototxt', dest = 'prototxt_file',
						help = 'The prototxt file to use - test.prototxt')
	parser.add_argument('--model', dest = 'caffe_model',
						help = 'The caffe model file to use - *.caffemodel')
	parser.add_argument('--test_iter', dest = 'test_iter',
						help = 'Number of testing iteration')
	parser.add_argument('--cond', dest = 'cond',
						help = 'Testing condition')
	parser.add_argument('--difficulty', dest = 'difficulty',
						help = 'Testing difficulty')
	args = parser.parse_args()
	return args

def detect(net, classes, test_path, test_iter, cond, difficulty):

	CONF_THRESH = 0.8
	NMS_THRESH = 0.3
	del_order = ['easy', 'medium', 'hard']
	pr_bboxes = np.zeros((1,7))
	# iterate files and do detection
	test_set_file = test_path + '/test.txt'
	with open(test_set_file) as f:
		lines = f.readlines()
	# iterate each line in file and process one image at a time
	for img_idx in range(len(lines)):
		curr_line = lines[img_idx]
		curr_line = curr_line.strip('\n')
		# read image files by name
		im_file = os.path.join(test_path + '/' + '../TestImages/' + curr_line + '.jpg')
		print ('Testing image: ', im_file)
		im = cv2.imread(im_file)
		im_draw = Image.open(im_file)
		# do actual detection and get scores and bounding boxes
		scores, boxes = im_detect(net, im)
		# do nms and other processing for storing detections
		for cls in classes:
			cls_ind = CLASSES.index(cls)
			cls_boxes = boxes[:, 4*cls_ind:4*(cls_ind + 1)]
			cls_scores = scores[:, cls_ind]
			keep = np.where(cls_scores >= CONF_THRESH)[0]
			cls_boxes = cls_boxes[keep, :]
			cls_scores = cls_scores[keep]
			# dets is bbox
			dets = np.hstack((cls_boxes, cls_scores[:, np.newaxis])).astype(np.float32)
			keep = nms(dets, NMS_THRESH)
			dets = dets[keep, :]
			print "after NMS dets are:"
			print dets
			# add for precision=recall computation
			temp_bboxes = []
			if dets.size > 0:
				for ii in range(len(dets)):
					# ing_idx + 1 to keep matlab happy
					a = np.hstack((np.squeeze(dets[ii,:]), cls_ind, img_idx+1)).astype(np.float32)
					temp_bboxes.append(a)
			tl = len(temp_bboxes)
			l,x = np.shape(pr_bboxes)
			pr_bboxes = np.resize(pr_bboxes,[l+tl, 7])
			for jj in range(tl):
				pr_bboxes[l+jj,:] = temp_bboxes[jj]
			# finally save the result
			# save_det_images(im_draw, im_file, dets)
	print ('Going to save results')
	pr_dest = test_path + '/../predictions/pr_bboxes' + '_' + str(cond) + '_' + str(difficulty) + '_' + str(test_iter) + '.mat'
	sio.savemat(pr_dest, {'pr_bboxes' : pr_bboxes})


def save_det_images(im, im_file, dets, thresh = 0.5):
		"""Draw detected bounding boxes."""
		inds = np.where(dets[:, -1] >= thresh)[0]
		if len(inds) == 0:
			print('no target detected!')
			return
		draw = ImageDraw.Draw(im)
		print(dets)

		for det in dets:
			cor = (det[0],det[1],det[2],det[3])
			width = 5
			for i in range(width):
	   			draw.rectangle(cor, outline="red")
	   			cor = (cor[0]+1,cor[1]+1, cor[2]+1,cor[3]+1)

		_im_name = os.path.basename(im_file)
		im.save(os.path.join('/media/Data/shrini/dataset/predictions', _im_name), "JPEG")



if __name__ == '__main__':
	cfg.TEST.HAS_RPN = True
	# Parse all the arguments to the file
	args = parse_args()
	caffe.set_mode_gpu()
	caffe.set_device(args.gpu_id)

	net = caffe.Net(args.prototxt_file, args.caffe_model, caffe.TEST)
	print '\n\nLoaded network successfully \n\n'

	# do the detection and save results as *.mat file
	detect(net, ('object_0','object_1','object_2','object_3','object_4',), \
		   args.test_path, \
		   args.test_iter, \
		   args.cond, \
		   args.difficulty)
