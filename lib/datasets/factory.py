# --------------------------------------------------------
# Fast R-CNN
# Copyright (c) 2015 Microsoft
# Licensed under The MIT License [see LICENSE for details]
# Written by Ross Girshick
# --------------------------------------------------------

"""Factory method for easily getting imdbs by name."""

__sets = {}

from datasets.pascal_voc import pascal_voc
from datasets.coco import coco
from datasets.defo_object import defo_object

import numpy as np

# Set up voc_<year>_<split> using selective search "fast" mode
for year in ['2007', '2012']:
    for split in ['train', 'val', 'trainval', 'test']:
        name = 'voc_{}_{}'.format(year, split)
        __sets[name] = (lambda split=split, year=year: pascal_voc(split, year))

# Set up coco_2014_<split>
for year in ['2014']:
    for split in ['train', 'val', 'minival', 'valminusminival']:
        name = 'coco_{}_{}'.format(year, split)
        __sets[name] = (lambda split=split, year=year: coco(split, year))

# Set up coco_2015_<split>
for year in ['2015']:
    for split in ['test', 'test-dev']:
        name = 'coco_{}_{}'.format(year, split)
        __sets[name] = (lambda split=split, year=year: coco(split, year))


# Set up kaggle_<split> using selective search "fast" mode
# kaggle_devkit_path = '/home/coldmanck/kaggle'
# For the kaggle right whale dataset
#kaggle_devkit_path = '/home/isit/workspace/fast-rcnn/data/kaggle'
# MODIFIED for our dataset
# defo_object_devkit_path = '/home/isit/workspace/DefoObjInWild/dataset'
defo_object_devkit_path = '/home/shrinivasan/work/defoobjinwild/dataset'

for split in ['train', 'test']:
    # MODIFIED for our dataset
    #name = '{}_{}'.format('kaggle', split)
    #__sets[name] = (lambda split=split: datasets.kaggle(split, kaggle_devkit_path))
    name = '{}_{}'.format('defo_object', split)
    __sets[name] = (lambda split=split: defo_object(split, defo_object_devkit_path))

def get_imdb(name):
    """Get an imdb (image database) by name."""
    print(name)
    print(__sets)
    if not __sets.has_key(name):
        raise KeyError('Unknown dataset: {}'.format(name))
    return __sets[name]()

def list_imdbs():
    """List all registered imdbs."""
    return __sets.keys()
